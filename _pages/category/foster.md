---
layout: category
category:
  name: foster
  tags: [advocacy, process, activity, support]
  image: "foster.svg"
related:
  - title: "W3C Social Web Community Group"
    url: "https://www.w3.org/wiki/SocialCG"
  - title: "Fediverse Policy Special Interest Group"
    url: "https://socialhub.activitypub.rocks/c/meeting/fediverse-policy/59"
  - title: "Scaling Up Cooperation on SocialHub"
    url: "https://socialhub.activitypub.rocks/t/scaling-up-cooperation/837"
permalink: "/foster.html"
---
We can be very proud of how Fediverse has been created in true grassroots fashion. However, in order to take our ecosystem to the next level we need to focus on community empowerment. Advocating our story, attract more contributors, encourage crowdsourcing. Here we focus on coordinated efforts, improving process and community organization.